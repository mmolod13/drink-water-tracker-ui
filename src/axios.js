// axios
import axios from 'axios'

export default axios.create({
    baseURL: "http://api"
    // You can add your headers here
})
