import store from "../store/store"

export default {

    // install plugin
    install(app) {

        /**
         * Dispatch action
         * @param action
         * @param payload
         * @returns {Promise<unknown>}
         */
        app.config.globalProperties.$dispatch = function (action, payload) {
            return new Promise((resolve, reject) => {
                store.dispatch(action, payload).then(res => {
                    // resolve promise
                    resolve(res);
                }).catch(err => {
                    // reject promise
                    reject(err);

                    console.log("Error processing: " + err)
                    // show error toast
                    //toast.notify(this, 'Error', err.message, 'danger');
                });
            });
        };

        /**
         * Getter
         * @param action
         * @param payload
         * @returns {*}
         */
        app.config.globalProperties.$get = function (action, payload) {
            // Get getters
            return payload !== undefined ? store.getters[action](payload) : store.getters[action];
        };

        /**
         * Unreact get
         * @param action
         * @param payload
         * @returns {*}
         */
        app.config.globalProperties.$unreactGet = function (action, payload = false) {

            // Get getters
            return this.$api.unreact(this.$get(action, payload));
        }
    }
};
