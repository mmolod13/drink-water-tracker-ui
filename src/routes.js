
// import HomePage from './pages/home.vue';
import StepperPage from './pages/drinkWaterStepper.vue';
import StepperResultPage from './pages/drinkWaterResult.vue';
import Login from './pages/login.vue';

var routes = [
  {
    path: '/',
    component: StepperPage,
  },
  {
    path: '/result',
    component: StepperResultPage,
  },

  // Login page
  {
    path: '/login',
    component: Login,
  },
];

export default routes;
