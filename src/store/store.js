// Import Vue
import { createStore } from "vuex";

import state from "./state"
import getters from "./getters"
import mutations from "./mutations"
import actions from "./actions"
import moduleAuth from './auth/moduleAuth.js'
import moduleStepper from './stepper/moduleStepper.js'

// Create store
export default createStore({
    getters,
    mutations,
    state,
    actions,
    modules: {
        auth: moduleAuth,
        stepper: moduleStepper
    },
    strict: process.env.NODE_ENV !== 'production'
})
