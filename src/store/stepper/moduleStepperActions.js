export default {
    drinkWaterData({commit}, data) {
        commit('UPDATE_DRINK_WATER_FIELD', data);
    }
}
