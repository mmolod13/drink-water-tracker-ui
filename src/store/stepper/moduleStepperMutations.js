export default {
    UPDATE_DRINK_WATER_FIELD(state, payload) {
        state.drinkWaterData = {...state.drinkWaterData, ...payload}
    }
}
