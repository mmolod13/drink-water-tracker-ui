export default {
    personsGender: state => {
        return state.drinkWaterData.gender;
    },
    personsWeight: state => {
        return `${state.drinkWaterData.weight.value || '--'} ${state.drinkWaterData.weight.unit}`;
    },
}
