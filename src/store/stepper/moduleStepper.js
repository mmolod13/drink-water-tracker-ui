import state from './moduleStepperState.js'
import actions from './moduleStepperActions.js'
import mutations from './moduleStepperMutations.js'
import getters from './moduleStepperGetters.js'

export default {
	namespaced: true,
    state: state,
    actions: actions,
    mutations: mutations,
    getters: getters
}
